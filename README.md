# klanra-code-test

This is a simple android app containing a single activity that shows sample weather data for user's location.

###Project setup
Clone the repo, open the project in Android Studio, hit "Run". Done!

###Architecture overview
The application implemented and structured based on **MVVM** architectural patterns following **Clean Architecture** practices.
Applications functionality is implemented in app module using **kotlin** programming language.
App module consists of domain, data, data source, presentation and common ui packages.

**LiveData** is used to propagate data from data to domain and, as well in presentation layer between View (activity) and ViewModel.
**Data Binding** is used to bind UI components in layouts to data sources.

###External dependencies
These are the external APIs/services that application depends on
- Darksky's Weather API

###3rd Party Libraries
- okHttp
- Moshi


