package com.konpach.klarnacodetest.dataSource.remote.api

import com.konpach.klarnacodetest.domain.model.Forecast

data class ForecastData(val summary: String, val temperature: Double)

fun ForecastData.mapToDomain(): Forecast = Forecast(summary, temperature)