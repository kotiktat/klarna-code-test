package com.konpach.klarnacodetest.dataSource.remote.api


data class ForecastDataResult(var currently: ForecastData)