package com.konpach.klarnacodetest.dataSource.remote

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.konpach.klarnacodetest.data.dataSource.ForecastRemoteDataSource
import com.konpach.klarnacodetest.dataSource.remote.api.ForecastDataResult
import com.konpach.klarnacodetest.dataSource.remote.api.mapToDomain
import com.konpach.klarnacodetest.domain.model.Forecast
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import okhttp3.*
import org.json.JSONException
import java.io.IOException

class ForecastRemoteDataSourceImpl : ForecastRemoteDataSource {
    val TAG = this::class.java.simpleName

    override fun getForecast(lat: Double, lng: Double): MutableLiveData<Forecast> {
        val result = MutableLiveData<Forecast>()
        val url = "https://api.darksky.net/forecast/2bb07c3bece89caf533ac9a5d23d8417/$lat,$lng"
        var client = OkHttpClient()
        var request = Request.Builder().url(url).build()
        val call = client.newCall(request)
        call.enqueue(
            object : Callback {
                override fun onResponse(call: Call?, response: Response) {
                    val responseData = response.body()?.string()
                    responseData?.run {
                        val moshi = Moshi.Builder().build()
                        val type = Types.getRawType(ForecastDataResult::class.java)
                        val resp = moshi.adapter(type).fromJson(this) as ForecastDataResult
                        result.postValue(resp.currently.mapToDomain())
                    }
                }

                override fun onFailure(call: Call?, e: IOException?) {
                    Log.d(TAG, "Request Failure.")
                }
            }
        )
        return result
    }

}