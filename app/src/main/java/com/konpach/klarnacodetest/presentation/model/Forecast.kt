package com.konpach.klarnacodetest.presentation.model

import com.konpach.klarnacodetest.domain.model.Forecast

data class ForecastItem(val summary: String, val temperature: Double)

fun Forecast.mapToPresentation(): ForecastItem = ForecastItem(summary, temperature)