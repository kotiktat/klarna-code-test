package com.konpach.klarnacodetest.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.konpach.klarnacodetest.data.repository.ForecastRepositoryImpl
import com.konpach.klarnacodetest.dataSource.remote.ForecastRemoteDataSourceImpl
import com.konpach.klarnacodetest.domain.useCase.ForecastUseCase
import com.konpach.klarnacodetest.presentation.model.ForecastItem
import com.konpach.klarnacodetest.presentation.model.mapToPresentation

class MainViewModel : ViewModel() {

    var location = MutableLiveData<Location>()

    private var forecastUseCase: ForecastUseCase =
        ForecastUseCase(ForecastRepositoryImpl(ForecastRemoteDataSourceImpl()))

    val forecast = Transformations.switchMap(location) {
        if (it != null) {
            forecastUseCase.getForecast(it.lat, it.lng)
        } else {
            MutableLiveData()
        }
    }

    fun fetchForecast(lat: Double, lng: Double) {
        location.postValue(Location(lat, lng))
    }

    val forecastItem: LiveData<ForecastItem> = Transformations.map(forecast) {
        it.mapToPresentation()
    }

    class Location(val lat: Double, val lng: Double)
}


