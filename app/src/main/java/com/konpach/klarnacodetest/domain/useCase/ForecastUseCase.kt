package com.konpach.klarnacodetest.domain.useCase

import com.konpach.klarnacodetest.domain.repository.ForecastRepository

class ForecastUseCase constructor(private val forecastRepository: ForecastRepository) {

    fun getForecast(lat: Double, lng: Double) = forecastRepository.getForecast(lat, lng)
}