package com.konpach.klarnacodetest.domain.model

data class Forecast(val summary: String, val temperature: Double)