package com.konpach.klarnacodetest.domain.repository

import androidx.lifecycle.MutableLiveData
import com.konpach.klarnacodetest.domain.model.Forecast

interface ForecastRepository {

    fun getForecast(lat: Double, lng: Double): MutableLiveData<Forecast>
}