package com.konpach.klarnacodetest.data.dataSource

import androidx.lifecycle.MutableLiveData
import com.konpach.klarnacodetest.domain.model.Forecast

interface ForecastRemoteDataSource{
    fun getForecast(lat: Double, lng: Double): MutableLiveData<Forecast>

}