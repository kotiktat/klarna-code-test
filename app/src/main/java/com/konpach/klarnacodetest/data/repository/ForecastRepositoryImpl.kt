package com.konpach.klarnacodetest.data.repository

import com.konpach.klarnacodetest.data.dataSource.ForecastRemoteDataSource
import com.konpach.klarnacodetest.domain.repository.ForecastRepository

class ForecastRepositoryImpl(private val forecastRemoteDataSource: ForecastRemoteDataSource) : ForecastRepository {
    override fun getForecast(lat: Double, lng: Double) = forecastRemoteDataSource.getForecast(lat, lng)
}